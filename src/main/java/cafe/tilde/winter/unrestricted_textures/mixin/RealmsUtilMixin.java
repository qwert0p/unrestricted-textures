package cafe.tilde.winter.unrestricted_textures.mixin;

import cafe.tilde.winter.unrestricted_textures.UnrestrictedTextureGetter;
import com.mojang.authlib.GameProfile;
import com.mojang.authlib.minecraft.MinecraftProfileTexture;
import com.mojang.authlib.minecraft.MinecraftSessionService;
import net.minecraft.client.realms.util.RealmsUtil;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Redirect;

import java.util.Map;

@Mixin(RealmsUtil.class)
public class RealmsUtilMixin {
    @Redirect(method = "getTextures", at = @At(value = "INVOKE", target = "Lcom/mojang/authlib/minecraft/MinecraftSessionService;getTextures(Lcom/mojang/authlib/GameProfile;Z)Ljava/util/Map;"))
    private static Map<MinecraftProfileTexture.Type, MinecraftProfileTexture> getTextures(
            MinecraftSessionService instance, GameProfile gameProfile, boolean b) {
        return UnrestrictedTextureGetter.getTextures(gameProfile);
    }
}
