package cafe.tilde.winter.unrestricted_textures.mixin;

import cafe.tilde.winter.unrestricted_textures.UnrestrictedTextureGetter;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Maps;
import com.mojang.authlib.GameProfile;
import com.mojang.authlib.legacy.LegacyMinecraftSessionService;
import com.mojang.authlib.minecraft.InsecureTextureException;
import com.mojang.authlib.minecraft.MinecraftProfileTexture;
import com.mojang.authlib.minecraft.MinecraftSessionService;
import com.mojang.blaze3d.systems.RenderSystem;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.texture.PlayerSkinProvider;
import net.minecraft.client.texture.TextureManager;
import net.minecraft.util.Identifier;
import net.minecraft.util.Util;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.ModifyArg;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import java.io.File;
import java.util.HashMap;

@Mixin(PlayerSkinProvider.class)
public abstract class PlayerSkinProviderMixin {
    @Final
    @Shadow
    private MinecraftSessionService sessionService;

    @Shadow
    protected abstract Identifier loadSkin(MinecraftProfileTexture texture, MinecraftProfileTexture.Type type,
                                           PlayerSkinProvider.SkinTextureAvailableCallback cb);

    @Inject(method = "loadSkin(Lcom/mojang/authlib/GameProfile;Lnet/minecraft/client/texture/PlayerSkinProvider$SkinTextureAvailableCallback;Z)V",
            at = @At("HEAD"),
            cancellable = true)
    public void loadSkin(GameProfile profile, PlayerSkinProvider.SkinTextureAvailableCallback callback,
                         boolean requireSecure, CallbackInfo ci) {
        Runnable runnable = () -> {
            HashMap<MinecraftProfileTexture.Type, MinecraftProfileTexture> map = Maps.newHashMap();
            try {
                map.putAll(UnrestrictedTextureGetter.getTextures(profile));
            }
            catch (InsecureTextureException ignored) {}
            if (map.isEmpty()) {
                profile.getProperties().clear();
                if (profile.getId().equals(MinecraftClient.getInstance().getSession().getProfile().getId())) {
                    profile.getProperties().putAll(MinecraftClient.getInstance().getSessionProperties());
                    map.putAll(UnrestrictedTextureGetter.getTextures(profile));
                } else {
                    this.sessionService.fillProfileProperties(profile, requireSecure);
                    try {
                        map.putAll(UnrestrictedTextureGetter.getTextures(profile));
                    }
                    catch (InsecureTextureException ignored) {}
                }
            }
            MinecraftClient.getInstance().execute(() -> RenderSystem.recordRenderCall(() -> ImmutableList.of(
                    MinecraftProfileTexture.Type.SKIN, MinecraftProfileTexture.Type.CAPE
            ).forEach(textureType -> {
                if (map.containsKey(textureType)) {
                    this.loadSkin(map.get(textureType), textureType, callback);
                }
            })));
        };
        Util.getMainWorkerExecutor().execute(runnable);
        ci.cancel();
    }
}
